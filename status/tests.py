from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import Status
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options


class status(TestCase):
	def test_status_landingpage_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_status_profil_url_is_exist(self):
		response = Client().get('/profil')
		self.assertEqual(response.status_code, 200)

	def test_add_status_url_is_exist(self):
		response = Client().get('/addstatus')
		self.assertEqual(response.status_code, 302)

	def test_delete_status_url_is_exist(self):
		response = Client().get('/deletestatus')
		self.assertEqual(response.status_code, 302)

	def test_status_berlangganan_url_is_exist(self):
		response = Client().get('/berlangganan')
		self.assertEqual(response.status_code, 200)

	def test_status_using_LandingPage_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'LandingPage.html')

	def test_status_using_Subscribe_template(self):
		response = Client().get('/berlangganan')
		self.assertTemplateUsed(response, 'Subscribe.html')

	def test_status_using_Profil_template(self):
		response = Client().get('/profil')
		self.assertTemplateUsed(response, 'Profil.html')

	def test_status_using_landingpage_func(self):
		found = resolve('/')
		self.assertEqual(found.func, landingpage)

	def test_status_using_addstatus_func(self):
		found = resolve('/addstatus')
		self.assertEqual(found.func, addstatus)

	def test_status_using_deletestatus_func(self):
		found = resolve('/deletestatus')
		self.assertEqual(found.func, deletestatus)

	def test_status_using_profil_func(self):
		found = resolve('/profil')
		self.assertEqual(found.func, profil)

	def test_statys_using_berhasil_func(self):
		found = resolve('/berhasil')
		self.assertEqual(found.func, berhasil)

	def test_status_using_berlangganan_func(self):
		found = resolve('/berlangganan')
		self.assertEqual(found.func, berlangganan)


	def test_status_has_heading_greet(self):
		greet = "Hello, Apakabar?"

		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(greet, html_response)

	def test_profil_has_greeting_text(self):
		greet = "HAI SAYA FIRRIYAL BIN YAHYA !"

		response = Client().get('/profil')
		html_response = response.content.decode('utf8')
		self.assertIn(greet, html_response)

	def test_feriyalstory_has_change_theme(self):
		changeTheme = "Ubah Tema"

		response  = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(changeTheme, html_response)

	def test_status_model_can_create_new_status(self):
		new_status = Status.objects.create(status='senang')

		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

	def test_subsciber_model_can_create_new_subscriber(self):
		new_subscriber = Subscriber.objects.create(nama="feriyal", email="cobacoba@gmail.com", password="cobacoba123")
		counting_all_available_subscriber = Subscriber.objects.all().count()
		self.assertEqual(counting_all_available_subscriber, 1)

	def test_form_status_input_has_placeholder_and_css_classes(self):
		form = StatusForm()
		self.assertIn('class="status-form-input"', form.as_p())
		self.assertIn('id="id_status"', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = StatusForm(data={'status':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['status'],["This field is required."])

	def test_status_post_success_and_render_the_result(self):
		test = 'bahagia'
		response_post = Client().post('/addstatus', {'status': test})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_status_post_error_and_render_the_result(self):
		test = 'Tidak ada status saat ini.'
		response_post = Client().post('/addstatus', {'status': ''})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)


class FeriyalStoryFunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FeriyalStoryFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FeriyalStoryFunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('https://feriyalstory.herokuapp.com')
        time.sleep(5)
        # find the form element
        status = selenium.find_element_by_id('id_status')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        status.send_keys('Coba Coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)

        self.assertIn("Coba Coba", selenium.page_source)

    def test_position_hello_apakabar(self):
    	selenium = self.selenium

    	selenium.get('https://feriyalstory.herokuapp.com')
    	time.sleep(5)
    	helloapakabar = selenium.find_element_by_tag_name('h1').text
    	self.assertIn("Hello, Apakabar?", helloapakabar)

    def test_position_status_terkini(self):
    	selenium = self.selenium
    	selenium.get('https://feriyalstory.herokuapp.com')
    	time.sleep(5)
    	statusterkini = selenium.find_element_by_tag_name('h3').text
    	self.assertIn("Status Terkini", statusterkini)

    def test_input_status_box_color(self):
    	selenium = self.selenium
    	selenium.get('https://feriyalstory.herokuapp.com')
    	input_box_color = selenium.find_element_by_class_name('box-list-status').value_of_css_property('background-color')
    	self.assertEqual(input_box_color, "rgba(255, 255, 255, 1)")

    def test_hello_apakabar_font(self):
    	selenium = self.selenium
    	selenium.get('https://feriyalstory.herokuapp.com')
    	helloapakabarfont = selenium.find_element_by_tag_name("h1").value_of_css_property('font-family')
    	self.assertIn('Handlee', helloapakabarfont)

    def test_changed_theme(self):
        selenium = self.selenium
        selenium.get('https://feriyalstory.herokuapp.com')
        time.sleep(5)
        ubahtema = selenium.find_element_by_id('ubahtema')
        ubahtema.click()

        backgroundcolor = selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
        font = selenium.find_element_by_tag_name('h1').value_of_css_property('font-family')
        self.assertIn('Mali', font)
        self.assertEqual(backgroundcolor, "rgba(217, 179, 140, 1)")

    def test_accordion_profil(self):
        selenium = self.selenium
        selenium.get('https://feriyalstory.herokuapp.com/profil')
        button = selenium.find_element_by_id('aktifitas')
        button.click()

        self.assertIn("Menuntut Ilmu Agama", selenium.page_source)
	






	
