from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'status'

urlpatterns = [
    path('', views.landingpage, name="landingpage"),
    path('addstatus', views.addstatus, name="addstatus"),
    path('deletestatus', views.deletestatus, name="deletestatus"),
    path('profil', views.profil, name="profil"),
    path('berlangganan', views.berlangganan, name="berlangganan"),
    path('pengecekan', views.pengecekan, name="pengecekan"),
    path('berhasil', views.berhasil, name="berhasil"),
    path('listsubscriber', views.listsubscriber, name="listsubscriber"),
    path('jsonsubscriber', views.jsonsubscriber, name="jsonsubscriber"),
    path('pengecekanpassword', views.pengecekanpassword, name="pengecekanpassword")
]