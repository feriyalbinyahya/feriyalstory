from django.db import models

class Status(models.Model):
	status = models.TextField(max_length=300)
	waktu = models.DateTimeField(auto_now_add=True, editable=False, null=False, blank=False)

class Subscriber(models.Model):
	nama = models.CharField(max_length=300)
	email = models.CharField(max_length=100)
	password = models.CharField(max_length=20)
