from django import forms
from .models import Status
from .models import Subscriber

class StatusForm(forms.Form):
	status_attrs = {
		'id': 'id_status',
		'type': 'text',
		'class': 'status-form-input',
		'placeholder': 'Apa yang anda rasakan?'
	}


	status = forms.CharField(label="", required=True, max_length=300, 
		widget=forms.TextInput(attrs=status_attrs))


class SubscriberForm(forms.Form):
	nama_attrs = {
		'id': 'id_nama',
		'type': 'text',
		'class': 'subscriber-form-input',
		'placeholder': ' Nama',
		'required': True
	}

	email_attrs = {
		'id': 'id_email',
		'type': 'email',
		'class': 'subscriber-form-input',
		'placeholder': ' Email',
		'required': True
	}

	password_attrs = {
		'id': 'id_password',
		'type': 'password',
		'class': 'subscriber-form-input',
		'placeholder': ' Password',
		'required': True,
		'min_length': 5
	}

	nama = forms.CharField(widget=forms.TextInput(attrs=nama_attrs))
	email = forms.CharField(widget=forms.EmailInput(attrs=email_attrs))
	password= forms.CharField(widget=forms.PasswordInput(attrs=password_attrs))