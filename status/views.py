from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import Status
from .forms import SubscriberForm
from .models import Subscriber
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers



# Create your views here.
response = {}
def landingpage(request):    
    response['author'] = "" #TODO Implement yourname
    allstatus = Status.objects.all()
    response['allstatus'] = allstatus
    html = 'LandingPage.html'
    response['status_form'] = StatusForm
    return render(request, html, response)

def addstatus(request):
    form = StatusForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        perasaan = Status(status=response['status'])
        perasaan.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')


def deletestatus(request):
    Status.objects.all().delete()
    return HttpResponseRedirect('/')

def profil(request):
    return render(request, 'Profil.html')

def berlangganan(request):
    response['author'] = ""
    response['subscriber_form'] = SubscriberForm
    return render(request, "Subscribe.html", response)


@csrf_exempt
def pengecekan(request):
    email = request.POST.get("email")
    data = {'not_valid': Subscriber.objects.filter(email__iexact=email).exists()}
    return JsonResponse(data)

@csrf_exempt
def pengecekanpassword(request):
    password = request.POST.get("passwordd")
    email = request.POST.get("email")
    cek_subscriber =  Subscriber.objects.get(email=email)
    print(cek_subscriber.password)
    terhapus = False;
    if cek_subscriber.password == password:
        terhapus = True;
        cek_subscriber.delete()

    data = {'terhapus': terhapus, 'nama':cek_subscriber.nama}
    return JsonResponse(data)

def berhasil(request):
    submited_form = SubscriberForm(request.POST or None)
    if (submited_form.is_valid()):
        cd = submited_form.cleaned_data
        new_subscriber = Subscriber(nama=cd['nama'], password=cd['password'], email=cd['email'])
        new_subscriber.save()
        data = {'nama': cd['nama']}
    return JsonResponse(data)

def listsubscriber(request):
    response = {}
    
    return render(request, "ListSubscriber.html", response)

def jsonsubscriber(request):
    data_json = serializers.serialize('json', Subscriber.objects.all())
    return HttpResponse(data_json, content_type='aplication/json')

