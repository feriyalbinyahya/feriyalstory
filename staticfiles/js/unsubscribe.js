$(document).ready(function() {
    $.ajax({
        url: "/jsonsubscriber",
        method: "GET",
        dataType: 'json',
        success: function(data) {
            for(i = 0; i< data.length; i++) {
                var subs = "<div id =\"" + data[i].fields.email + "\" class=\"subs\"><div class=\"subs-body\"><h3 class=\"subs-title\">" +data[i].fields.nama+ "</h3><button type=\"button\" class=\"unsubscribe btn btn-danger\">Unsubscribe</button></div></div>";
                $("#result").append(subs);
            }


            var pengecekan_password = false;
            $(".unsubscribe").each(function() {
                $(this).click(function() {
                    var emailnya = $(this).closest('.subs').attr('id');
                    (async function getPassword () {
                        const {value: password} = await swal({
                          title: 'Masukkan password',
                          input: 'password',
                          inputPlaceholder: 'Masukkan password',
                          inputAttributes: {
                            maxlength: 20,
                            autocapitalize: 'off',
                            autocorrect: 'off'
                          }
                        })

                        console.log(password);
                        console.log(emailnya);
                        if (password) {
                            $.ajax({
                                url: "/pengecekanpassword",
                                data: {
                                    passwordd: password, email: emailnya
                                },
                                method: "POST",
                                dataType: 'json',
                                success: function(data) {
                                    console.log(data)
                                    if(data.terhapus) {
                                        swal({
                                            title: "Berhasil",
                                            text: data.nama + ' telah membatalkan berlangganan',
                                            type: 'success',
                                            showConfirmButton: false,
                                            timer: 2500
                                            }).then(function() {
                                                window.location = "/listsubscriber"
                                            });
                                    } else {
                                        swal({
                                            type: 'error',
                                            title: 'Password tidak sesuai',
                                            text: 'Mohon isi password dengan benar',
                                            });
                                    } 
                                }
                            });
                        }
                    })()
                });
            });   
        }
    });
    
});