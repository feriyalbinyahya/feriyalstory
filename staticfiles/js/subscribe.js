$(document).ready(function() {
    var nama = "";
    var password = "";
    var pengecekan_nama = false;
    var pengecekan_password = false;
    var pengecekan_email = false;
    $("#id_nama").change(function() {
        nama = $(this).val();

        if(nama.length > 0){
        	pengecekan_nama = true;
        }else{
        	pengecekan_nama = false;
        }

        if(pengecekan_email && pengecekan_password && pengecekan_nama){
        	$("#subscribe").removeAttr("disabled");
        }else{
        	$("#subscribe").attr("disabled", "disabled");
        }
    });
    $("#id_password").change(function() {
        password = $(this).val();

        if(password.length>=5){
        	pengecekan_password = true;
        }else{
        	pengecekan_password = false;
        }

        if(pengecekan_email && pengecekan_password && pengecekan_nama){
        	$("#subscribe").removeAttr("disabled");
        }else{
        	$("#subscribe").attr("disabled", "disabled");
        }
    })
    $("#id_email").change(function() {
        var email = $(this).val();
        console.log(email)
        $.ajax({
            url: "/pengecekan",
            data: {
                'email': email,
            },
            method: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data)
                if(data.not_valid) {
                	console.log("masuk pak eko")
                    $("#email-message").css("display", "block");
                } else {
                    if(email.length > 0){
                    	pengecekan_email = true;
                    	$("#email-message").css("display", "none");
                    }else{
                    	pengecekan_email = false;
                    }


                    if(pengecekan_email && pengecekan_password && pengecekan_nama){
        				$("#subscribe").removeAttr("disabled");
        			}else{
        				$("#subscribe").attr("disabled", "disabled");
        			}
                }
                
            }
        });
    });
    $("#subscribe").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url:"/berhasil",
            data: $("form").serialize(),
            method: 'POST',
            dataType: 'json',
            success: function(data){
                swal({
                    title: "Hai " + data.nama,
                    text: 'Terima kasih sudah berlangganan pada website kami. Kembali kehalaman utama...',
                    type: 'success',
                    showConfirmButton: false,
                    timer: 2500
                    }).then(function() {
                        window.location = "/"
                    });   
            }, error: function() {
                swal({
                    type: 'error',
                    title: 'Gagal',
                    text: 'Ada yang salah !',
                  }).then(function() {
                      window.location = "/"
                  });
            }
        })
    });
});
