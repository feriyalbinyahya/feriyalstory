from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

class store(TestCase):
	def test_store_buku_url_is_exist(self):
		response = Client().get('/buku')
		self.assertEqual(response.status_code, 200)

	def test_store_using_Buku_template(self):
		response = Client().get('/buku')
		self.assertTemplateUsed(response, 'Buku.html')

	def test_store_using_buku_func(self):
		found = resolve('/buku')
		self.assertEqual(found.func, buku)

	def test_store_using_counterFavBuku_func(self):
		found = resolve('/counterFavBuku')
		self.assertEqual(found.func, counterFavBuku)

	def test_status_has_heading_greet(self):
		greet = "Daftar Buku"

		response = Client().get('/buku')
		html_response = response.content.decode('utf8')
		self.assertIn(greet, html_response)

