from django.shortcuts import render
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.core import serializers
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
import requests
import json

response = {}
response['counter'] = 0
def dataBuku(request, judul):
	URL = "https://www.googleapis.com/books/v1/volumes?q=" + judul
	get_json = requests.get(URL)
	return HttpResponse(get_json, content_type='application/json')

def buku(request):
	response['counter'] = 0
	if request.user.is_authenticated:
		if 'counter' not in request.session:
			request.session['counter'] = 0
		if 'nama' not in request.session:
			request.session['nama'] = request.user.username
		if 'email' not in request.session:
			request.session['email'] = request.user.email

		count = request.session.get('counter', 0)
		response['counter'] = count
	return render(request, "Buku.html", response)

@csrf_exempt
def counterFavBuku(request):
	if request.user.is_authenticated:
		request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type='application/json')

def logoutView(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/buku')