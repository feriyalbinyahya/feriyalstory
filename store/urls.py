from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'store'

urlpatterns = [
    path('buku', views.buku, name="buku"),
    path('dataBuku/<judul>', views.dataBuku, name="databuku"),
    path('counterFavBuku', views.counterFavBuku, name="counterFavBuku"),
    path('logout/', views.logoutView, name="logout"),
]
